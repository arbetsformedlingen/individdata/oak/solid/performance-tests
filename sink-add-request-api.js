import { SolidNodeClient } from 'solid-node-client';
import { readFileSync } from 'fs';
import * as solid from './solid.js';
import express from "express";
import fetch from 'node-fetch';

function getTestCase() {
  return (process.argv.length > 2) ? process.argv[2] : 0;
}

const testCase = parseInt(getTestCase());

const podUrlAppend = ((baseUrl, user) => `${baseUrl}/${user}`);

const podUrlSubdomain = ((baseUrl, user) => {
  const sepIndex = baseUrl.indexOf("://");
  const protocol = baseUrl.substring(0, sepIndex);
  const server = baseUrl.substring(sepIndex + 3);
  return `${protocol}://${user}.${server}`
});

function podParams(testCase) {
  switch (testCase) {
    case 0: return {
      baseUrl: "http://localhost:3000",
      podUrl: podUrlAppend,
      pod: "sink",
      data: {
        source: "source",
        user: "user",
        sink: "sink"
      }
    }
    case 1: return {
      baseUrl: "https://css-ipo-dev.test.services.jtech.se",
      podUrl: podUrlAppend,
      pod: "sink",
      data: {
        source: "source",
        user: "user",
        sink: "sink"
      }
    }
    case 2: return {
      baseUrl: "https://css2-ipo-dev.test.services.jtech.se",
      podUrl: podUrlAppend,
      pod: "sink",
      data: {
        source: "source",
        user: "user",
        sink: "sink"
      }
    }
    case 3: return {
      baseUrl: "https://solidcommunity.net",
      podUrl: podUrlSubdomain,
      pod: "sink10",
      data: {
        source: "source10",
        user: "user10",
        sink: "sink10"
      }
    }
    case 4: return {
      baseUrl: "https://pod.inrupt.com",
      podUrl: podUrlAppend,
      pod: "sinktest10",
      data: {
        source: "source",
        user: "user",
        sink: "sinktest10"
      }
    }
  }
}


const pod = podParams(testCase).pod;
const source = podParams(testCase).data.source;
const user = podParams(testCase).data.user;
const sink = podParams(testCase).data.sink;
const podUrl = ((user) => podParams(testCase).podUrl(podParams(testCase).baseUrl, user));
console.log("podUrl: ", podUrl(pod));

const dataRequest = ((id, pnr) => `
<> a <http://oak.se/UnemploymentCertificateDataRequest>;
    <http://schema.org/identifier> "${id}";
    <http://purl.org/dc/elements/1.1/subject> <${podUrl(user)}/profile/card#me>;
    <https://oak.se/subjectId> "${pnr}";
    <http://schema.org/sourceOrganization> <${podUrl(source)}/profile/card#me>;
    <http://www.w3.org/2007/ont/link#requestedBy> <${podUrl(sink)}/profile/card#me>.
`);

const dataRequestAcl = ((id) => `
<#user> a <http://www.w3.org/ns/auth/acl#Authorization>;
    <http://www.w3.org/ns/auth/acl#accessTo> <${podUrl(user)}/oak/requests/request-${id}>;
    <http://www.w3.org/ns/auth/acl#agent> <mailto:${user}@example.com>, <${podUrl(user)}/profile/card#me>;
    <http://www.w3.org/ns/auth/acl#mode> <http://www.w3.org/ns/auth/acl#Read>.
<#owner> a <http://www.w3.org/ns/auth/acl#Authorization>;
    <http://www.w3.org/ns/auth/acl#accessTo> <${podUrl(sink)}/oak/requests/request-${id}>;
    <http://www.w3.org/ns/auth/acl#agent> <mailto:${sink}@example.com>, <${podUrl(sink)}/profile/card#me>;
    <http://www.w3.org/ns/auth/acl#mode> <http://www.w3.org/ns/auth/acl#Write>, <http://www.w3.org/ns/auth/acl#Read>, <http://www.w3.org/ns/auth/acl#Control>.
`);

const dataRequestPath = (pod, id) => `${podUrl(pod)}/oak/requests/request-${id}`;
console.log("dataRequestPath: ", dataRequestPath(pod, "[GUID]"));
const dataRequestAclPath = (pod, id) => dataRequestPath(pod, id) + '.acl';
console.log("dataRequestAclPath: ", dataRequestAclPath(pod, "[GUID]"));

// Log in to pod
const credentials = JSON.parse(readFileSync(`./sink-credentials-${testCase}.json`));
const client = new SolidNodeClient();
let session
try {
  session  = await client.login(credentials);
} catch (err) {
  console.log("err: ", err);
  process.exit(1);
}
console.log("Logged in as: ", session.webId);

const guids = readFileSync('./guid.txt').toString('utf8');
const guid = guids.split("\n");

async function createRequest(id) {
  const p1 = solid.putFile(session, dataRequestPath(pod, id), dataRequest(id, "870820-5441"), 'text/turtle');
  const p2 = solid.putFile(session, dataRequestAclPath(pod, id), dataRequestAcl(id), 'text/turtle');
  await Promise.all([p1, p2]);
  return id;
}

async function readRequest(id) {
  const p1 = solid.getFile(session, dataRequestPath(pod, id), 'text/turtle');
  const p2 = solid.getFile(session, dataRequestAclPath(pod, id), 'text/turtle');
  await Promise.all([p1, p2]);
  return id;
}

async function deleteRequest(id) {
  const p1 = solid.deleteFile(session, dataRequestPath(pod, id));
  const p2 = solid.deleteFile(session, dataRequestAclPath(pod, id));
  await Promise.all([p1, p2]);
  return id;
}

const app = express();
app.use(express.json());
var request = 0;
app.get("/write", async(req, res) => {
  request++;
  const id = await createRequest(guid[request]);
  res.end(`{id: ${id}`);
});
app.get("/read", async(req, res) => {
  request++;
  const id = await readRequest(guid[request]);
  res.end(`{id: ${id}`);
});
app.get("/delete", async(req, res) => {
  request++;
  const id = await deleteRequest(guid[request]);
  res.end(`{id: ${id}`);
});
app.get("/nil", async(req, res) => {
  res.end(`{id: nil}`);
});
app.get("/solidproxy/:path", async(req, res) => {
  const baseUrl = podParams(testCase).baseUrl;
  const params = req.params;
  const path =  params.path;
  const response = await session.fetch(baseUrl + "/" + path);
  const body = await response.text();
  res.end(body);
});
app.get("/fetchproxy/:path", async(req, res) => {
  const baseUrl = podParams(testCase).baseUrl;
  const params = req.params;
  const path =  params.path;
  const response = await fetch(baseUrl + "/" + path);
  const body = await response.text();
  res.end(body);
});
app.get("/reset", async(req, res) => {
  request = 0;
  res.end("Counter reset to reference first GUID in guid.txt file.\n\n");
});
app.get("/stop", async(req, res) => {
  res.end("Stopping sink-add-request-api service.");
  process.exit(0);
});

console.log("Starting sink-add-request-api service on port 9090");
app.listen(9090);
