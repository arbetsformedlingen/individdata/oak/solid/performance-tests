/**
 * A collection of utilities to read and write from a pod
 */

/**
 * Put a file in a pod.  Throws on error.
 * @param {Session} session The logged-in session
 * @param {string} file The path to the file to write
 * @param {string} body The contents of the file
 * @param {string} contentType The content-type of the file
 */
 export async function putFile(session, file, body, contentType = 'text/plain') {
    //console.log("file: ", file);
    let writeResponse = await session.fetch(file, {
        method: "PUT",
        body: body,
        headers: { "Content-type": contentType }
    });
    if(!(writeResponse.status >= 200 && writeResponse.status <= 299)) {
        console.log("Failed to write the file: " + file + "writeResponse.status: " + writeResponse.status);
        //throw new Error("Failed to write the file: " + file);
    }
}

/**
 * Get a document from a pod and return it as an object. Throws on error.
 * @param {Session} session The logged-in session
 * @param {string} file The path to the file to read
 * @param {string} contentType MIME to accept
 * @returns The data and contentType
 */
 export async function getFile(session, file, contentType = 'application/ld+json') {
    const response = await session.fetch(file, {
        headers: { "Accept": contentType }
    });
    //console.log("GET: " + response.url + " status: " + response.status);
    if (response.status !== 200) {
        console.log("Failed to read the file: " + file + "response.status: " + response.status);
        //throw new Error("Failed to read the file: " + file);
    }
    const body = await response.text();
    return {data: body, contentType: response.headers.get("Content-Type")};
}

/**
 * Delete a file from a pod.  Throws on error.
 * @param {Session} session The logged-in session
 * @param {string} file The path to the file to delete
 */
 export async function deleteFile(session, file) {
//    const ok = new Set([202, 204, 205, 404]);
    const ok = new Set([200, 202, 204, 205, 404]); //solidcommunity.net returns 200 for successful DEL!!!
    let response = await session.fetch(file, {
        method: "DELETE"});
//    console.log("DELETE: " + response.url + " status: " + response.status);
    if(!ok.has(response.status)) {
        console.log("Failed to delete the file: " + file + response.statusText);
        //throw new Error("Failed to delete the file: " + file);
    }

//    console.log("DELETE: " + deleteResponse.url);
}
