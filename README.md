# Solid Server performance tests
With these scripts different deployments of the Community Solid Server have been stress tested.

The scripts have been inspired by the https://gitlab.com/arbetsformedlingen/individdata/oak/solid-client-node-experimentation repository.

## Test casses
0. A locally deployed Community Solid Server at http://localhost:3000.
1. A Community Solid Server from https://github.com/solid/community-server.git@v2.0.1 deployed in Jobtechdevs OpenShift environment with a default configuration, memory based storage, residing on eu-central-1.compute.amazonaws.com in Frankfurt
2. A Community Solid Server from https://github.com/individdata/community-server.git@c75a90809073a7e00f03eb26e69b598828f36985 deployed in Jobtechdevs OpenShift environment, configured with notification handling. And a memory based storage, residing on eu-central-1.compute.amazonaws.com in Frankfurt

## Scripts
**solid.js** is a utlity script file wrapping the PUT, GET and DELETE methods for creating, reading and deleting resources from the pod server as a logged in client.

**setup-pods.js** were used to create users for the Community Solid Server with the default configuration without any persistent storage.

**gen-token.js** is a script for token creation by automating the username/password login.

**sink-add-request-api.js** is a tiny API server that makes it possible to load test the pod server with [AutoCannon](https://github.com/mcollina/autocannon). It relies on the prefilled **guid.txt** file with 1000 pre generated GUIDs, and **sink-credential-[N].json** files where access tokens are stored for the logged in client.

## Usage
1. Generate token for the test case. To generate the token for thest case 1 run:
<pre>node gen-token 1</pre>
This will create a token that will be stored in the sink-credentials-1.json file.

2. Start the api server for a test case. For test case 1 run:
<pre>node sink-add-request-api 1&</pre>
This starts the api server at localhost:9090, by which resources for the successive GUIDs in the guid.txt file can be written, read and deleted at the server for the test case.
The api is STATEFUL, so every new call to the api will point to the next GUID in the guid.txt file. The api has the folowing GET paths defined:

- http://localhost:9090/reset resets the state of the GUID counter to point to the first GIUD in guid.txt
- http://localhost:9090/write writes a resource and its corresponding acl for the current GUID and updates the GUID pointer to the next GUID
- http://localhost:9090/read reads a resource and its corresponding acl for the current GUID and updates the GUID pointer to the next GUID
- http://localhost:9090/delete deletes a resource and its corresponding acl for the current GUID and updates the GUID pointer to the next GUID
- http://localhost:9090/stop stops the server

3. You can use CURL to write/read/delete to make a single request for test purposes that will write aresource and an acl-resource for the current GUID. For example:
<pre>curl http://localhost:9090/write</pre>

4. If not already installed, install AutoCannon.
5. Run stress tests with AutoCannon tat will do read requests for 500 consecutive GUIDs in the guid.txt file with 16 concurrent clients:
<pre>autocannon -c 16 -a 500 http://localhost:9090/read</pre>
6. Stop the server:
<pre>curl http://localhost:9090/stop</pre>
